function arr = read_data(filename, XDIM, YDIM, ZDIM)

fileID = fopen(filename);
arr = fread(fileID, XDIM * YDIM * ZDIM, 'float', 'ieee-be');

% set ground to zero
arr(arr > 1e10) = 0;

% 1D array -> 3D cube
arr = reshape(arr, YDIM, XDIM, ZDIM);

% Y axis is negative
arr = flip(arr,1);

end

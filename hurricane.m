function hurricane()
    % dimensions for all datasets
    DIM1 = 500;
    DIM2 = 500;
    DIM3 = 100;
    
    % wind subsample factor
    SUBSAMPLE = 50;
    
    %slider units
    slider_units = 'km';
    
    %slider case
    slider_case = 1;
    
    % load data
    TCf = read_data('data/TCf01.bin', DIM1, DIM2, DIM3);  % temperature
    Uf = read_data('data/Uf01.bin', DIM1, DIM2, DIM3);   % wind x-component
    Vf = read_data('data/Vf01.bin', DIM1, DIM2, DIM3);   % wind y-component
    Wf = read_data('data/Wf01.bin', DIM1, DIM2, DIM3);   % wind z-component
    
    % set default values for top view
    T = TCf;
    X = Uf;
    Y = Vf;
    Z = Wf;
    
    XDIM = DIM1;
    YDIM = DIM2;
    ZDIM = DIM3;
    
    level = 1;
    
    % ---- UI ----
    % create figure
    fig = figure('Units', 'normal', ...
        'NumberTitle', 'off', 'Name', 'Hurricane Isabel Visualization');
    
    % load slices for default level
    [X_slice, Y_slice, T_slice] = get_data_for_level(1);
     
    ax = axes(fig, 'units', 'normalized',...
      'position', [0.1, 0.17, 0.75, 0.75]);

    % paint temperature
    pc = pcolor(ax, 1:YDIM,1:XDIM, T_slice);    
      
    update_axes_ticks(1);

    % set constant min and max values for temperature colormap
    caxis([min(T(:)) max(T(:))]);
    
    % add colorbar at the bottom
    c = colorbar('southoutside');
    c.Label.String = 'Temperature (°C)';
    
    % set temperature colormap
    colormap(parula);
    
    % disable frames around each temperature cell
    shading interp
    
    % hold on so that we can paint wind arrows on top of temperature
    hold on;
    
    % paint arrows
    q = quiver(1:YDIM/SUBSAMPLE:YDIM,1:XDIM/SUBSAMPLE:XDIM, X_slice, Y_slice, 1, ...
    'Color','black');

    % add slider
    slider=uicontrol('style','slider','units', 'normalized',...
      'position',[0.92 0.27 0.03 0.68],...
      'min',1,'max',ZDIM,'value',1, ...
      'callback',@slider_callback);
    
    % add text "slider_value"
    slider_val=uicontrol('style','text',...
        'units', 'normalized',...
        'position',[0.88 0.95 0.1 0.035],...
        'String','0.035km');
    
    % add text "view"
    view_button_txt = uicontrol('style','text',...
        'units', 'normalized',...
        'Position',[0.02 0.05 0.1 0.035], ...
        'String', 'view');
    
    % add pop-up list for view
    view_button = uicontrol('style','pop',...
                 'units', 'normalized', ...
                 'position',[0.12 0.055 0.2 0.035],...
                 'string',{'top';'front'},...
                 'value',1, ...
                 'callback',@view_callback);
    
    % add text "colormap"
    view_button_txt = uicontrol('style','text',...
        'units', 'normalized',...
        'Position',[0.38 0.05 0.1 0.035], ...
        'String', 'colormap');
             
    % add pop-up list for colormap
    colormap_button = uicontrol('style','pop',...
                 'units', 'normalized', ...
                 'position',[0.49 0.055 0.2 0.035],...
                 'string',{'parula';'hot';'cool';'jet';'spring';'colorcube'},...
                 'value',1, ...
                 'callback',@colormap_callback);
             
    animation_button = uicontrol('style', 'pushbutton', ...
                   'units', 'normalized', ...
                 'position',[0.78 0.038 0.2 0.06],...
                 'String', 'Animation',...
                 'callback',@animation_callback);


    function animation_callback(src, ~)
        orig_level = level;
        for i = 1:(ZDIM/10):ZDIM
            level = i;            
            set(slider, 'Value', i);
            redraw_ui();
        end
        level = orig_level;
        set(slider, 'Value', orig_level);
    end

    % callback for slider event
    function slider_callback(src, ~)
        level = round(src.Value); 
        redraw_ui();
    end
    
    function a = adjust_slider_scale(x,number) 
        switch x
            case 1
                a = number*0.2-0.165;
            case 2
                a = number*0.036+23.7;
        end
    end

    % callback for colormap event
    function colormap_callback(src, ~)
       switch src.Value
           % parula
           case 1
            colormap(parula);
           % hot
           case 2
            colormap(hot);
           % cool
           case 3
            colormap(cool);
           % jet
           case 4
            colormap(jet);
           % spring
           case 5
            colormap(spring);
           %colorcube
           case 6
            colormap(colorcube);
       end
       redraw_ui();
            
    end

    % callback for view event
    function view_callback(src, ~)
       switch src.Value
           % top
           case 1
               T = TCf;
               X = Uf;
               Y = Vf;
               Z = Wf;

               XDIM = DIM1;
               YDIM = DIM2;
               ZDIM = DIM3;
               
               slider_units = 'km';
               slider_case = 1;
           % front
           case 2
               perm = [3 1 2];
               T = flip(permute(TCf, perm), 2);
               X = flip(permute(Uf, perm), 2);
               Y = flip(permute(Wf, perm), 2);
               Z = flip(permute(Vf, perm), 2);

               XDIM = DIM3;
               YDIM = DIM1;
               ZDIM = DIM2;
               
               slider_units = 'N';
               slider_case = 2;
       end
       set(slider, 'value', 1);
       set(slider, 'max', ZDIM);
       update_axes_ticks(src.Value);
       redraw_ui();
            
    end

    function [X_slice, Y_slice, T_slice] = get_data_for_level(level)
        % get all slices for given level
        
        T_slice = get_slice(T,level);
        X_slice = get_slice(X,level);
        Y_slice = get_slice(Y,level);

        X_slice = X_slice(1:XDIM/SUBSAMPLE:end, 1:YDIM/SUBSAMPLE:end);
        Y_slice = Y_slice(1:XDIM/SUBSAMPLE:end, 1:YDIM/SUBSAMPLE:end);
    end

    function redraw_ui()
        delete(q);
        delete(pc);        
        [X_slice, Y_slice, T_slice] = get_data_for_level(level);
        
        pc = pcolor(1:YDIM,1:XDIM,T_slice);
        axis([0 YDIM 0 XDIM]);
        shading interp;
        
        q = quiver(1:YDIM/SUBSAMPLE:YDIM,1:XDIM/SUBSAMPLE:XDIM, X_slice, Y_slice, 1, ...
        'Color','black');
        
        slider_val.String = strcat(num2str(adjust_slider_scale(slider_case, slider.Value)),slider_units);
    
        drawnow;
    end

    function update_axes_ticks(val)
        switch val
            case 1                
                set(ax,'XTick', [1 YDIM/2 YDIM]);
                set(ax,'XTickLabels', [83  72.5 62]);

                set(ax,'YTick', [1 XDIM/2 XDIM]);
                set(ax,'YTickLabels', [23.7 32.7 41.7]); 
                
            case 2
                set(ax,'XTick', [1 YDIM/2 YDIM]);
                set(ax,'XTickLabels', [83  72.5 62]);

                set(ax,'YTick', [1 XDIM/2 XDIM]);
                set(ax,'YTickLabels', [.035  9.535 19.835]); 
        end
    end
end